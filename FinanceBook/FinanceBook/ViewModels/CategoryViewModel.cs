﻿using FinanceBook.Interfaces;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using FinanceBook.Models;
using System.Collections.ObjectModel;
using System.Linq;
namespace FinanceBook.ViewModels
{
  public  class CategoryViewModel
    {
        private SQLiteConnection database;
        private static object collisionLock = new object();
        public ObservableCollection<Categories> _Categories { get; set; }
        public CategoryViewModel() {
         
            database = DependencyService.Get<IDatabaseConnection>().DbConnection();
            database.CreateTable<Categories>();
        }
        public void AddCategory(Categories categories)
        {


            this._Categories = new ObservableCollection<Categories>(database.Table<Categories>());
            if (categories.Id > 0)
            {
               
                database.Update(categories);


            }
            else {
                categories.AlowEdit = true;
                this._Categories.Add(categories);
                database.Insert(categories);
            }
            
          
        }

        public IEnumerable<Categories> GetCategories(Enums.Category categoryType)
        {
            lock (collisionLock)
            {
               
                var query = from fin in database.Table<Categories>()
                            select fin;

               // database.DeleteAll<Categories>();
                if (query.ToList().Count == 0) {

          
                    AddCategory(new Categories { Id = 0, Name = "Ingreso propio",Details ="ejemplo sueldo", Type = (int)Enums.Category.CATEGORY_INCOME, AlowEdit= false });
                    AddCategory(new Categories { Id = 0, Name = "Servicios ", Details="abcdesddf", Type = (int)Enums.Category.CATEGORY_INCOME, AlowEdit = false });
                    AddCategory(new Categories { Id = 0, Name = "Articulos", Details = "los articulos que sean", Type = (int)Enums.Category.CATEGORY_INCOME, AlowEdit = false });
                    AddCategory(new Categories { Id = 0, Name = "Activo fijo", Details = "los activos fijos", Type = (int)Enums.Category.CATEGORY_INCOME, AlowEdit = false });
                  
                    AddCategory(new Categories { Id = 0, Name = "Electricidad", Details ="Energia electrica", Type = (int)Enums.Category.CATEGORY_EXPENSE, AlowEdit = false });
                    AddCategory(new Categories { Id = 0, Name = "Comunicacion", Details ="telefono,cable,internet",Type = (int)Enums.Category.CATEGORY_EXPENSE, AlowEdit = false });
                    AddCategory(new Categories { Id = 0, Name = "Alimentos", Details ="piazza , pan , arroz" , Type = (int)Enums.Category.CATEGORY_EXPENSE, AlowEdit = false });
                    AddCategory(new Categories { Id = 0, Name = "Vestimenta", Details ="pantalon, tshirt, jeans",Type = (int)Enums.Category.CATEGORY_EXPENSE, AlowEdit = false });
                    AddCategory(new Categories { Id = 0, Name = "Empleados", Details = "juan, miguel, jose", Type = (int)Enums.Category.CATEGORY_EXPENSE , AlowEdit = false });
                    AddCategory(new Categories { Id = 0, Name = "Tranfer", Details = "tranferencias", Type = (int)Enums.Category.CATEGORY_INCOME , AlowEdit = false });
                    AddCategory(new Categories { Id = 0, Name = "Tranfer", Details = "tranferencias", Type = (int)Enums.Category.CATEGORY_EXPENSE , AlowEdit = false });
                    query = from fin in database.Table<Categories>()
                            select fin;
                }
              
                var rs = query.AsEnumerable().Where(c => c.Type == (int)categoryType);
                
                return rs;
            }
        }
    }
}
