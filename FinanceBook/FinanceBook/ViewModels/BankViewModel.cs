﻿using FinanceBook.Interfaces;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using FinanceBook.Models;
using System.Collections.ObjectModel;
using System.Linq;

namespace FinanceBook.ViewModels
{
    public class BankViewModel
    {
        private SQLiteConnection database;
        private static object collisionLock = new object();
        public ObservableCollection<Bank> _Banks { get; set; }
        public BankViewModel() {
            database = DependencyService.Get<IDatabaseConnection>().DbConnection();
            database.CreateTable<Bank>();
        }
        public void Add(Bank bank) {
            this._Banks = new ObservableCollection<Bank>(database.Table<Bank>());
            if (bank.Id > 0)
            {
                database.Update(bank);

            }
            else {
                this._Banks.Add(bank);
                database.Insert(bank);
            }
         
            
        }
        public IEnumerable<Bank> GetBanks()
        {
            lock (collisionLock)
            {

                var query = from fin in database.Table<Bank>()
                            select fin;
              //  database.DeleteAll<Bank>();
                if (query.ToList().Count == 0)
                {
                    Add(new Bank { Id =0 , Name = "EFECTIVO" ,Date =DateTime.Now});
                    Add(new Bank { Id = 0 ,Name = "BANCO 1", Date = DateTime.Now });
                   
                }
              return  query.AsEnumerable();
            }
           
        }
    }
}
