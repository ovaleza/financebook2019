﻿using FinanceBook.Interfaces;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using FinanceBook.Models;
using System.Collections.ObjectModel;
using System.Linq;
namespace FinanceBook.ViewModels
{
    public class FinanceEntryViewModel
    {
        private SQLiteConnection database;
        private static object collisionLock = new object();
        public ObservableCollection<FinanceEntry> FinanceEntrys { get; set; }
        public FinanceEntryViewModel()
        {
               
            database = DependencyService.Get<IDatabaseConnection>().DbConnection();
            database.CreateTable<FinanceEntry>();
            // If the table is empty, initialize the collection
            if (!database.Table<FinanceEntry>().Any())
            {
                // AddNewCustomer();
            }
        }

        public void AddFinanceEntry(FinanceEntry financeEntry) {
            
            
            this.FinanceEntrys = new ObservableCollection<FinanceEntry>(database.Table<FinanceEntry>());
            if (financeEntry.Id > 0) {
             
                database.Update(financeEntry);
            }
            else {
                this.FinanceEntrys.Add(financeEntry);
                database.Insert(financeEntry);
            }
           
         
            GetFinanceEntry();


        }

        public IEnumerable<FinanceEntry> GetFinanceEntry()
        {
            lock (collisionLock)
            {
                var query = from fin in database.Table<FinanceEntry>()
                            select fin;
                   return query.AsEnumerable();
            }
        }

        public decimal GetLastBalance(DateTime date , Enums.FinanceEntry financeEntry ) {
            decimal lastBalance = 0;
            lock (collisionLock)
            {
                var query = from fin in database.Table<FinanceEntry>()
                            where fin.Type == (int)financeEntry & fin.Date < date
                            select fin;
                lastBalance=  query.AsEnumerable().Sum(x =>x.Amount);
                if (financeEntry == Enums.FinanceEntry.EXPENSE) {
                    lastBalance = lastBalance * -1;
                }
                if (financeEntry == Enums.FinanceEntry.BOTH)
                {
                    var IncomeAmount = database.Table<FinanceEntry>().Where(s => s.Type == (int)Enums.FinanceEntry.INCOME && s.Date < date).Sum(e => e.Amount);
                    var ExpensesAmount = database.Table<FinanceEntry>().Where(s => s.Type == (int)Enums.FinanceEntry.EXPENSE && s.Date < date).Sum(e => e.Amount);
                    lastBalance = IncomeAmount - ExpensesAmount;

                }

                return lastBalance;

            }
        }
    }
}
