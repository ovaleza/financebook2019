﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using FinanceBook.Models;
using FinanceBook.ViewModels;
using XamForms.Controls;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;
using FinanceBook.Dtos;
using System.Globalization;

namespace FinanceBook
{


    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FinanceMainPage : TabbedPage
    {
        public PlotModel BarModel { get; set; }
        int FinanceEntryId = 0;
        Enums.Category category;
        Entry Name = new Entry { Placeholder = "Nombre Categoria" };
        Entry Comment = new Entry { Placeholder = "Descripcion de la categoria" };
        int CategoryId = 0;
        int Categoryedit = 0;
        IconEntry.FormsPlugin.Abstractions.IconEntry NameBank = new IconEntry.FormsPlugin.Abstractions.IconEntry { Placeholder = "Nombre Banco" , Icon = "bank.png" };
        IconEntry.FormsPlugin.Abstractions.IconEntry ReferenceBank = new IconEntry.FormsPlugin.Abstractions.IconEntry { Placeholder = "Referencia", Icon = "reference.png" };
        IconEntry.FormsPlugin.Abstractions.IconEntry Balance = new IconEntry.FormsPlugin.Abstractions.IconEntry { Placeholder = "Balance", Icon= "balance.png" };
        IconEntry.FormsPlugin.Abstractions.IconEntry CommentBank = new IconEntry.FormsPlugin.Abstractions.IconEntry { Placeholder = "Comentarios" , Icon = "comentaries.png" };
        Grid grid = new Grid();
        Grid gridCategory = new Grid();
        ListView listViewBank = new ListView();
        DatePicker dateBank = new DatePicker { };
        DateTime dtIncome = DateTime.Now;
        DateTime dtExpenses = DateTime.Now;
        List<MyItem> MyItems = new List<MyItem>();
        ListView listEdit = new ListView();
        Button btnCatAdd = new Button
        {
            Text = "Agregar",
            TextColor = Color.White,
            Image = "save2.png",
            BackgroundColor = Color.FromHex("7FA9E7")
        };
        ListView list = new ListView();
        int BankId { get; set; }
        public FinanceMainPage()
        {
            InitializeComponent();
            //LoadCategory();
            LoadBanks();
            list.ItemTemplate = new DataTemplate(typeof(TextCell));
            list.ItemTemplate.SetValue(TextCell.TextProperty, "Name");
            list.ItemTemplate.SetBinding(TextCell.TextProperty, "Name");
            list.ItemTemplate.SetValue(TextCell.TextColorProperty, Color.Black);
            list.ItemTemplate.SetBinding(TextCell.DetailProperty, "Details");
            list.ItemTemplate.SetBinding(TextCell.DetailProperty, new Binding("Details"));
     
            DateTime dt = DateTime.Now;

            loadData();
            //  chartView.Chart. = 14;
        }
        private PlotModel CreatePieChart()
        {
            var model = new PlotModel { Title = "World population by continent" };

            var ps = new PieSeries
            {
                StrokeThickness = .25,
                InsideLabelPosition = .25,
                AngleSpan = 360,
                StartAngle = 0
            };
          

            ps.Slices.Add(new PieSlice("Africa", 1030) { IsExploded = false });
            ps.Slices.Add(new PieSlice("Americas", 929) { IsExploded = false });
            ps.Slices.Add(new PieSlice("Asia", 4157));
            ps.Slices.Add(new PieSlice("Europe", 739) { IsExploded = false });
            ps.Slices.Add(new PieSlice("Oceania", 35) { IsExploded = false });
            model.Series.Add(ps);
            return model;
        }
        private async void Button_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Amount.Text))
            {
                await DisplayAlert("Control Financiero", "Error " + "Debe indicar un monto", "Cancelar");
                Amount.Focus();
                return;
            }
            if (CategoryId ==0)
            {
                await DisplayAlert("Control Financiero", "Error " + "Debe indicar una categoria", "Cancelar");
                Amount.Focus();
                return;
            }
            string selectBank = bank1Income.SelectedItem.ToString();
            BankViewModel modelbank = new BankViewModel();
            int IdBank = modelbank.GetBanks().First(b => b.Name == selectBank).Id;
            int IdBank2 = 0;
            if (bank2Income.IsVisible) {
                string selectBank2 = bank2Income.SelectedItem.ToString();
                IdBank2 = modelbank.GetBanks().First(b => b.Name == selectBank2).Id;
            }
                //string d = Comment.
            FinanceEntry financeEntry = new FinanceEntry
            {
                Amount = Convert.ToDecimal(Amount.Text),
                Category = CategoryId,
                Comment = Commentario.Text,
                Type = 1,
                Date = DateIncome.Date,
                Bank1 = IdBank,
                Id = FinanceEntryId,
                Bank2 = IdBank2
            };
            FinanceEntryViewModel finance = new FinanceEntryViewModel();
            try
            {
                finance.AddFinanceEntry(financeEntry);
                await DisplayAlert("Control Financiero", "Agregado correctamente", "Ok");
                Amount.Text = string.Empty;
                Commentario.Text = string.Empty;
                Cat.Text = string.Empty;
                // btnSelectCatIn.Text = "Categoria: ________________________";
                CategoryId = 0;
                if (FinanceEntryId > 0) {
                    FinanceEntryId = 0;
                    this.CurrentPage = this.Children[2];
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Control Financiero", "Error " + ex.ToString(), "Cancelar");

            }
            loadData();

        }

        private async void btnExpenses_Clicked(object sender, EventArgs e)
        {
            
            if ( string.IsNullOrEmpty( AmountE.Text)) {
                await DisplayAlert("Control Financiero", "Error " + "Debe indicar un monto", "Cancelar");
                AmountE.Focus();
                return;
            }
            if (CategoryId == 0)
            {
                await DisplayAlert("Control Financiero", "Error " + "Debe indicar una categoria", "Cancelar");
                Amount.Focus();
                return;
            }
            string selectBank = bank1Expenses.SelectedItem.ToString();
            BankViewModel modelbank = new BankViewModel();
            int IdBank = modelbank.GetBanks().First(b => b.Name == selectBank).Id;
            int IdBank2 = 0;
            if (bank2Expenses.IsVisible)
            {
                string selectBank2 = bank2Expenses.SelectedItem.ToString();
                IdBank2 = modelbank.GetBanks().First(b => b.Name == selectBank2).Id;
            }
            FinanceEntry financeEntry = new FinanceEntry
            {
                Amount = Convert.ToDecimal(AmountE.Text),
                Category = CategoryId,
                Comment = CommentarioE.Text,
                Type = 2,
                Date = DateExpense.Date,
                Bank1 = IdBank,
                Id = FinanceEntryId
                ,Bank2 = IdBank2
            };
            FinanceEntryViewModel finance = new FinanceEntryViewModel();
         

            try
            {
                finance.AddFinanceEntry(financeEntry);
                await DisplayAlert("Control Financiero", "Agregado correctamente", "Ok");
                AmountE.Text = string.Empty;
                CommentarioE.Text = string.Empty;
                CatExpenses.Text = string.Empty;
                  // if (category == Enums.Category.CATEGORY_INCOME)
                  //  btnSelectCatIn.Text = "Categoria: ________________________";
                  //  else
                  //      btnSelectCatEx.Text = "Categoria: ________________________";

                  CategoryId = 0;
                if (FinanceEntryId > 0)
                {
                    FinanceEntryId = 0;
                    this.CurrentPage = this.Children[2];
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Control Financiero", "Error " + ex.ToString(), "Cancelar");

            }

            loadData();
        }




        private void btnAddcatIncome_Clicked(object sender, EventArgs e)
        {
            category = Enums.Category.CATEGORY_INCOME;
            Navigation.PushModalAsync(AddCategory());


        }

        private void btnAddcatExp_Clicked(object sender, EventArgs e)
        {
            category = Enums.Category.CATEGORY_EXPENSE;
            Navigation.PushModalAsync(AddCategory());
        }
        private ContentPage AddCategory()
        {
            var btnAddNew = new Button
            {
                Text = "Nuevo",
                TextColor = Color.Black,
                Image = "downarrow2.png",
                BackgroundColor = Color.Transparent,

            };
            btnAddNew.Clicked += btnNewCatShow_Clicked;
            btnCatAdd.IsEnabled = true;
            CategoryViewModel model = new CategoryViewModel();
           
            Comment.Text = string.Empty;
            Name.Text = string.Empty;
            Name.IsVisible = false;
            Comment.IsVisible = false;
           gridCategory.IsVisible = false;
            Categoryedit = 0;
            Label labelTitle = new Label { TextColor = Color.White, Margin = 1, Text = "Categorias", FontSize = 24, HorizontalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold, BackgroundColor = Color.FromHex("#50BFBF") };
            Label labelCatsSeparators = new Label { TextColor = Color.White, Text = "Categorias Agregadas", FontSize = 14, HorizontalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold, BackgroundColor = Color.FromHex("#50BFBF") };
            var btnCancel = new Button
            {
                Text = "Cancelar",
                TextColor = Color.White,
                Image = "remove.png",
                BackgroundColor = Color.FromHex("DB4E3F"),
                //  IsVisible = false
            };
            btnCancel.Clicked += btnAddCancelPoppout_Clicked;
            btnCancel.BorderRadius = 15;
            btnCatAdd.BorderRadius = 15;
            listEdit.ItemTemplate = new DataTemplate(typeof(TextCell));
            listEdit.ItemTemplate.SetValue(TextCell.TextProperty, "Name");
            listEdit.ItemTemplate.SetBinding(TextCell.TextProperty, "Name");
            listEdit.ItemTemplate.SetValue(TextCell.TextColorProperty, Color.Black);
            listEdit.ItemTemplate.SetBinding(TextCell.DetailProperty, "Details");
            listEdit.ItemTemplate.SetBinding(TextCell.DetailProperty, new Binding("Details"));
            listEdit.ItemsSource = model.GetCategories(category);
            listEdit.ItemSelected += OnSelectionEdit;
            btnCatAdd.Clicked += btnAdd_Clicked;
            gridCategory.RowDefinitions.Add(new RowDefinition { Height = 50 });
            gridCategory.RowDefinitions.Add(new RowDefinition { Height = 50 });
            gridCategory.ColumnDefinitions.Add(new ColumnDefinition { });
            gridCategory.ColumnDefinitions.Add(new ColumnDefinition { });
            gridCategory.Children.Add(btnCatAdd, 0, 0);
            gridCategory.Children.Add(btnCancel, 1, 0);
            var customPage = new ContentPage
            {
                Title = "Categorias",

                Content = new StackLayout
                {
                    Spacing = 5,
                    Padding = 30,
                    AnchorX = 40,
                    AnchorY = 40,
                    VerticalOptions = LayoutOptions.Center,
                    Children = {
                        labelTitle,
                        btnAddNew,
            Name,
            Comment,
           gridCategory,
           labelCatsSeparators,
           listEdit
            }
                }
            };
            return customPage;
        }
        private void btnNewCatShow_Clicked(object sender, EventArgs e) {
            bool visible = false;
            if (Name.IsVisible)
            {
                visible = false;
            }
            else
            {
                visible = true;
            }
        
            Name.IsVisible = visible;
            Comment.IsVisible = visible;
            gridCategory.IsVisible = visible;
        }
        private ContentPage SelectCategoryView()
        {
            CategoryViewModel model = new CategoryViewModel();
            //  list = new ListView();
            list.ItemsSource = model.GetCategories(category);
            list.SeparatorVisibility = SeparatorVisibility.Default;
            list.SeparatorColor = Color.Black;
            list.ItemSelected += OnSelection;
            var btnCancel = new Button
            {
                Text = "Cancelar",
                TextColor = Color.White,
                Image = "remove.png",
                BackgroundColor = Color.FromHex("DB4E3F"),
                //  IsVisible = false
            };
            btnCancel.Clicked += btnAddCancelPoppout_Clicked;
            var btn = new Button
            {
                Text = "Agregar",
                TextColor = Color.White,
                Image = "floppydisk.png",
                BackgroundColor = Color.FromHex("77D065")
            };
            btn.Clicked += btnAdd_Clicked;
            Label labelTitle = new Label { TextColor = Color.White, Margin = -1, Text = "Seleccione una categoria", FontSize = 24, HorizontalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold, BackgroundColor = Color.FromHex("#50BFBF") };
            var customPage = new ContentPage
            {
                Title = "Categorias",

                Content = new StackLayout
                {
                    Padding = 0
                    ,Margin = -1,

                   // VerticalOptions = LayoutOptions.CenterAndExpand,
                    Children = {
labelTitle,
            list,
            btnCancel


            }
                }
            };
            return customPage;
        }
        async void OnSelectionEdit(object sender, SelectedItemChangedEventArgs e)
        {

            var cat = (Categories)e.SelectedItem;
            Comment.Text = cat.Details;
            Name.Text = cat.Name;
            Categoryedit = cat.Id;
            btnCatAdd.IsEnabled = cat.AlowEdit;
            bool visible = false;
            if (Name.IsVisible)
            {
                visible = false;
            }
            else
            {
                visible = true;
            }

            Name.IsVisible = visible;
            Comment.IsVisible = visible;
            gridCategory.IsVisible = visible;

        }
        async void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            var cat = (Categories)e.SelectedItem;
            //   DisplayAlert("Item Selected", e.SelectedItem.ToString(), "Ok");
            CategoryId = cat.Id;
            if (category == Enums.Category.CATEGORY_INCOME)
            {
                Cat.Text = "Categoria: " + cat.Name;
                if (cat.Name == "Tranfer")
                {
                    bank2Income.IsVisible = true;
                    btnBank2Add.IsVisible = true;
                }
                else
                {
                    bank2Income.IsVisible = false;
                    btnBank2Add.IsVisible = false;
                }
            }
            else
            {
                if (cat.Name == "Tranfer")
                {
                    bank2Expenses.IsVisible = true;
                }
                else
                {
                    bank2Expenses.IsVisible = false;
                }
                CatExpenses.Text = "Categoria: " + cat.Name;
            }
            try
            {
                await Navigation.PopModalAsync(false);
            }
            catch (Exception)
            {

                //    throw;
            }

            //((ListView)sender).SelectedItem = null; //uncomment line if you want to disable the visual selection state.
        }
        private void btnAdd_Clicked(object sender, EventArgs e)
        {
            CategoryViewModel categoryViewModel = new CategoryViewModel();
            categoryViewModel.AddCategory(new Categories { Id = Categoryedit, Name = Name.Text, Type = (int)category, Details = Comment.Text });
            Name.Text = string.Empty;
            Comment.Text = string.Empty;
            CategoryViewModel model = new CategoryViewModel();
            listEdit.ItemTemplate = new DataTemplate(typeof(TextCell));
            listEdit.ItemTemplate.SetValue(TextCell.TextProperty, "Name");
            listEdit.ItemTemplate.SetBinding(TextCell.TextProperty, "Name");
            listEdit.ItemTemplate.SetValue(TextCell.TextColorProperty, Color.Black);
            listEdit.ItemTemplate.SetBinding(TextCell.DetailProperty, "Details");
            listEdit.ItemTemplate.SetBinding(TextCell.DetailProperty, new Binding("Details"));
            listEdit.ItemsSource = model.GetCategories(category);
            list.ItemsSource = model.GetCategories(category);
            list.EndRefresh();
            //LoadCategory();
            //    Navigation.PopModalAsync(false);
            loadData();
        }

        private void btnSelectCatEx_Clicked(object sender, EventArgs e)
        {
            category = Enums.Category.CATEGORY_EXPENSE;
            Navigation.PushModalAsync(SelectCategoryView());


        }

        private void btnSelectCatIn_Clicked(object sender, EventArgs e)
        {
            category = Enums.Category.CATEGORY_INCOME;
            Navigation.PushModalAsync(SelectCategoryView());

        }
        private void DatePicker_DateSelected(object sender, DateChangedEventArgs e)
        {
        }

    
        private void Cat_Focused(object sender, FocusEventArgs e)
        {
            category = Enums.Category.CATEGORY_INCOME;
            Navigation.PushModalAsync(SelectCategoryView());
        }

        private void CatExpenses_Focused(object sender, FocusEventArgs e)
        {
            category = Enums.Category.CATEGORY_EXPENSE;
            Navigation.PushModalAsync(SelectCategoryView());
        }
        private void LoadBanks()
        {
            BankViewModel bankViewModel = new BankViewModel();
            List<string> values = new List<string>();
            var rs = bankViewModel.GetBanks();
            foreach (var item in rs)
            {
                if (item.Name !=null)
                values.Add(item.Name);
            }
            bank1Income.ItemsSource = values;
            bank1Income.SelectedIndex = 0;
            bank2Income.ItemsSource = values;
            bank2Income.SelectedIndex = 0;

            bank1Expenses.ItemsSource = values;
            bank1Expenses.SelectedIndex = 0;
            bank2Expenses.ItemsSource = values;
            bank2Expenses.SelectedIndex = 0;
            TypeBankFilter.ItemsSource = values;
        }
        private ContentPage AddBank()
        {
            BankViewModel model = new BankViewModel();
          
            listViewBank.ItemTemplate = new DataTemplate(typeof(TextCell));
            listViewBank.ItemTemplate.SetValue(TextCell.TextProperty, "Name");
            listViewBank.ItemTemplate.SetBinding(TextCell.TextProperty, "Name");
            listViewBank.ItemTemplate.SetBinding(TextCell.DetailProperty, new Binding("Balance"));

            Balance.Keyboard = Keyboard.Numeric;
            listViewBank.ItemsSource = model.GetBanks();
            var btnAddNew = new Button
            {
                Text = "Nuevo",
                TextColor = Color.Black,
                Image = "downarrow2.png",
                BackgroundColor = Color.Transparent,
                
            };
            btnAddNew.Clicked += btnAddBankShow_Clicked;
            var btn = new Button
            {
                Text = "Guardar",
                TextColor = Color.White,
                Image = "save2.png",
                BackgroundColor = Color.FromHex("7FA9E7"),
              // IsVisible = false 
            };
            btn.BorderRadius = 15;
            var btnCancel = new Button
            {
                Text = "Cancelar",
                TextColor = Color.White,
                Image = "remove.png",
                BackgroundColor = Color.FromHex("DB4E3F"),
              //  IsVisible = false
            };
            btnCancel.BorderRadius = 15;
            btnCancel.Clicked += btnAddCancelPoppout_Clicked;

            grid.RowDefinitions.Add(new RowDefinition { Height = 50 });
            grid.RowDefinitions.Add(new RowDefinition { Height = 50 });
            grid.ColumnDefinitions.Add(new ColumnDefinition { });
            grid.ColumnDefinitions.Add(new ColumnDefinition { });
            grid.Children.Add(btn, 0, 0);
            grid.Children.Add(btnCancel, 1, 0);
            NameBank.IsVisible = false;
            Balance.IsVisible = false;
            ReferenceBank.IsVisible = false;
            CommentBank.IsVisible = false;
            dateBank.IsVisible = false;
            grid.IsVisible = false;
        
            btn.Clicked += btnAddBank_Clicked;

            listViewBank.ItemSelected += OnSelectionBank;
       
      
            Label labelTitle = new Label {TextColor = Color.White,  Margin = 1, Text = "Bancos" , FontSize = 24 , HorizontalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold, BackgroundColor = Color.FromHex("#50BFBF") };
            Label labelBanksSeparators = new Label { TextColor = Color.White,  Text = "Bancos Agregados", FontSize = 14, HorizontalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold, BackgroundColor = Color.FromHex("#50BFBF") };
            var customPage = new ContentPage
            {
                Title = "Bancos",
                BackgroundColor = Color.FromHex("#F4FBFB"),
                Content = new StackLayout
                {
                    Spacing = 5,
                    Padding = 30,
                    AnchorX = 40,
                    AnchorY = 40,
                    VerticalOptions = LayoutOptions.Center,
                    Children = {
                        labelTitle,
                        btnAddNew,
            NameBank,
            Balance,
            ReferenceBank,
            CommentBank,
            dateBank,
          
           grid,
             labelBanksSeparators,
           listViewBank
            }
                }
            };
            return customPage;
        }
        private void btnAddCancelPoppout_Clicked(object sender, EventArgs e) {
            Navigation.PopModalAsync(false);
        }
        private void btnAddBankShow_Clicked(object sender, EventArgs e)
        {
            bool visible = false;
            if (NameBank.IsVisible)
            {
                visible = false;
            }
            else {
                visible = true;
            }
            NameBank.IsVisible = visible;
            Balance.IsVisible = visible;
            ReferenceBank.IsVisible = visible;
            CommentBank.IsVisible = visible;
            dateBank.IsVisible = visible;
            grid.IsVisible = visible;
            loadData();

        }
            private void btnAddBank_Clicked(object sender, EventArgs e)
        {
            BankViewModel model = new BankViewModel();
            if (string.IsNullOrEmpty(Balance.Text)) {
                Balance.Text = "0";
            }
            model.Add(new Bank { Id = BankId, Name = NameBank.Text, Balance = Convert.ToDecimal(Balance.Text), Date = dateBank.Date, Reference = ReferenceBank.Text, Comment = CommentBank.Text });
            NameBank.Text = string.Empty;
            CommentBank.Text = string.Empty;
            dateBank.Date = DateTime.Now;
            Balance.Text = string.Empty;
            ReferenceBank.Text = string.Empty;
           

            listViewBank.ItemTemplate = new DataTemplate(typeof(TextCell));
            listViewBank.ItemTemplate.SetValue(TextCell.TextProperty, "Name");
            listViewBank.ItemTemplate.SetBinding(TextCell.TextProperty, "Name");
         //   listViewBank.ItemTemplate.SetValue(TextCell.DetailProperty, "Balance");
            listViewBank.ItemTemplate.SetBinding(TextCell.DetailProperty, new Binding("Balance"));

         
            listViewBank.ItemsSource = model.GetBanks();
            LoadBanks();
            NameBank.IsVisible = false;
            Balance.IsVisible = false;
            ReferenceBank.IsVisible = false;
            CommentBank.IsVisible = false;
            dateBank.IsVisible = false;
            grid.IsVisible = false;
           

        }
        async void OnSelectionBank(object sender, SelectedItemChangedEventArgs e)
        {
            var bank = (Bank)e.SelectedItem;
            NameBank.Text = bank.Name;
            BankId = bank.Id;
            CommentBank.Text = bank.Comment;
            Balance.Text = bank.Balance.ToString();
            ReferenceBank.Text = bank.Reference;
            dateBank.Date = bank.Date;
            bool visible = false;
            if (NameBank.IsVisible)
            {
                visible = false;
            }
            else
            {
                visible = true;
            }
            NameBank.IsVisible = visible;
            Balance.IsVisible = visible;
            ReferenceBank.IsVisible = visible;
            CommentBank.IsVisible = visible;
            dateBank.IsVisible = visible;
            grid.IsVisible = visible;

        }
        private void btnBank1Add_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(AddBank());
        }

     
        private PlotModel CreateBarChart(bool stacked, string title)
        {
            var model = new PlotModel
            {
                Title = title,
                LegendPlacement = LegendPlacement.Outside,
                LegendPosition = LegendPosition.BottomCenter,
                LegendOrientation = LegendOrientation.Horizontal,
                LegendBorderThickness = 0
            };
            
            var s1 = new BarSeries { Title = "Series 1", IsStacked = stacked, StrokeColor = OxyColors.Black, StrokeThickness = 1 };
            s1.Items.Add(new BarItem { Value = 25 });
            s1.Items.Add(new BarItem { Value = 137 });
            s1.Items.Add(new BarItem { Value = 18 });
            s1.Items.Add(new BarItem { Value = 40 });

            var s2 = new BarSeries { Title = "Series 2", IsStacked = stacked, StrokeColor = OxyColors.Black, StrokeThickness = 2 };
            s2.Items.Add(new BarItem { Value = 12 });
            s2.Items.Add(new BarItem { Value = 14 });
            s2.Items.Add(new BarItem { Value = 120 });
            s2.Items.Add(new BarItem { Value = 26 });

            var categoryAxis = new CategoryAxis { Position = CategoryAxisPosition()};
            categoryAxis.Labels.Add("Category A");
            categoryAxis.Labels.Add("Category B");
            categoryAxis.Labels.Add("Category C");
            categoryAxis.Labels.Add("Category D");
            var valueAxis = new LinearAxis { Position = ValueAxisPosition(), MinimumPadding = 0, MaximumPadding = 0.06, AbsoluteMinimum = 0 };
            model.Series.Add(s1);
            model.Series.Add(s2);
            model.Axes.Add(categoryAxis);
            model.Axes.Add(valueAxis);
            return model;
        }
        private AxisPosition CategoryAxisPosition()
        {
            if (typeof(BarSeries) == typeof(ColumnSeries))
            {
                return AxisPosition.Bottom;
            }

            return AxisPosition.Left;
        }

        private AxisPosition ValueAxisPosition()
        {
            if (typeof(BarSeries) == typeof(ColumnSeries))
            {
                return AxisPosition.Left;
            }

            return AxisPosition.Bottom;
        }

        private void Picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool visible = false;
            var s = (Xamarin.Forms.Picker)sender;
            if (s.SelectedIndex == 4)
            {
                visible = true;
            }
            DateStart.IsVisible = visible;
            DateEnd.Date = DateTime.Now.AddDays(1);
            DateEnd.IsVisible = visible;
        }

        private void btnFilters_Clicked(object sender, EventArgs e)
        {
            bool visible = false;
            if (FinanceEntryType.IsVisible)
            {
                visible = false;
            }
            else {
                visible = true;
            }
            FinanceEntryType.IsVisible = visible;
         //   TypeCategoryFilter.IsVisible = visible;
            TypeBankFilter.IsVisible = visible;
            TypeBankRange.IsVisible = visible;
            GridButton.IsVisible = visible;
        }

      
        private List<string> GetPikerCategory(Enums.Category category)
        {
            ViewModels.CategoryViewModel categorymodel = new CategoryViewModel();
            List<string> list = new List<string>();

            var rs = categorymodel.GetCategories(category);
            foreach (var item in rs)
            {
                list.Add(item.Name);
            }
            return list;
        }

    
        private void FinanceEntryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool visible = false;
            var s = (Xamarin.Forms.Picker)sender;
            if (s.SelectedIndex != 2)
            {
                visible = true;
                int select = s.SelectedIndex + 1;
                TypeCategoryFilter.ItemsSource = GetPikerCategory((Enums.Category)Convert.ToInt32(select));
            }
            TypeCategoryFilter.IsVisible = visible;
        }

        private void btnSearch_Clicked(object sender, EventArgs e)
        {
            loadData();
        }
        private void loadData() {
            List<Microcharts.Entry> Incomes = new List<Microcharts.Entry>();
            List<Microcharts.Entry> Expenses = new List<Microcharts.Entry>();
            List<Microcharts.Entry> Comparaties = new List<Microcharts.Entry>();
            List<string> colors = new List<string>();
            DateTime today = DateTime.Today;
            int currentDayOfWeek = (int)today.DayOfWeek;
            DateTime sunday = today.AddDays(-currentDayOfWeek);
            DateTime monday = sunday.AddDays(1);
            // If we started on Sunday, we should actually have gone *back*
            // 6 days instead of forward 1...
            if (currentDayOfWeek == 0)
            {
                monday = monday.AddDays(-7);
            }
            var dates = Enumerable.Range(0, 7).Select(days => monday.AddDays(days)).ToList();
            FinanceEntryViewModel viewModel = new FinanceEntryViewModel();
            var rs = viewModel.GetFinanceEntry().ToList();
            List<FinanceEntryDto> listFinance = new List<FinanceEntryDto>();


            if (FinanceEntryType.SelectedIndex != 2 && FinanceEntryType.SelectedIndex != -1)
            {
                rs = rs.Where(x => x.Type == FinanceEntryType.SelectedIndex + 1).ToList();
            }




            if (TypeBankRange.SelectedIndex == 0)
            {
                var tempList = rs.Where(e => e.Type == (int)Enums.FinanceEntry.INCOME
              && (e.Date.Day == DateTime.Now.Day || e.Date.Day == DateTime.Now.Day - 1)).GroupBy(x => x.Date.Day).OrderBy(r => r.First().Date).ThenBy(r2 => r2.First().Category).Select(cl => new Microcharts.Entry(float.Parse(cl.First().Amount.ToString()))
              {

                  Label = "Ingreso Dia :" + cl.First().Date.Day,
                  ValueLabel = String.Format("{0:n}", cl.Sum(d => d.Amount)),

              });
                var tempList2 = rs.Where(e => e.Type == (int)Enums.FinanceEntry.INCOME
             && (e.Date.Day == DateTime.Now.Day || e.Date.Day == DateTime.Now.Day - 1)).GroupBy(x => x.Date.Day).OrderBy(r => r.First().Date).ThenBy(r2 => r2.First().Category).Select(cl => new Microcharts.Entry(float.Parse(cl.First().Amount.ToString()))
             {

                 Label = "Gastos Dia :" + cl.First().Date.Day,
                 ValueLabel = String.Format("{0:n}", cl.Sum(d => d.Amount) * -1)
       ,
             });

                foreach (var item in tempList)
                {
                    var random = new Random();
                    var color = String.Format("#{0:X6}", random.Next(0x1000000));
                    while (colors.Contains(color))
                    {
                        color = String.Format("#{0:X6}", random.Next(0x1000000));
                    }
                    colors.Add(color);
                    Comparaties.Add(new Microcharts.Entry(float.Parse(item.ValueLabel))
                    {
                        Color = SkiaSharp.SKColor.Parse(color),
                        Label = item.Label,
                        ValueLabel = item.ValueLabel,
                    });
                }
                foreach (var item in tempList2)
                {
                    var random = new Random();
                    var color = String.Format("#{0:X6}", random.Next(0x1000000));
                    while (colors.Contains(color))
                    {
                        color = String.Format("#{0:X6}", random.Next(0x1000000));
                    }
                    colors.Add(color);
                    Comparaties.Add(new Microcharts.Entry(float.Parse(item.ValueLabel))
                    {
                        Color = SkiaSharp.SKColor.Parse(color),
                        Label = item.Label,
                        ValueLabel = item.ValueLabel,
                    });
                }
                rs = rs.Where(x => x.Date.Day == DateTime.Now.Day).ToList();
            }
            else if (TypeBankRange.SelectedIndex == 1)
            {
                List<FinanceEntry> financeEntries = new List<FinanceEntry>();
                DayOfWeek weekStart = DayOfWeek.Monday; // or Sunday, or whenever
                DateTime startingDate = DateTime.Today;

                while (startingDate.DayOfWeek != weekStart)
                    startingDate = startingDate.AddDays(-1);

                DateTime previousWeekStart = startingDate.AddDays(-7);
                DateTime previousWeekEnd = startingDate.AddDays(-1);
                int lastWeek = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(previousWeekStart, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
                int CuurentWeek = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstDay, DayOfWeek.Monday);

                var tempList = rs.Where(e => e.Type == (int)Enums.FinanceEntry.INCOME
                && (CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(e.Date, CalendarWeekRule.FirstDay, DayOfWeek.Monday)
                == lastWeek || CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(e.Date, CalendarWeekRule.FirstDay, DayOfWeek.Monday) == CuurentWeek)).GroupBy(x => (CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(x.Date, CalendarWeekRule.FirstDay, DayOfWeek.Monday))).OrderBy(r => r.First().Date).ThenBy(r2 => r2.First().Category).Select(cl => new Microcharts.Entry(float.Parse(cl.First().Amount.ToString()))
                {

                    Label = "Ingreso Semana :" + CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(cl.First().Date, CalendarWeekRule.FirstDay, DayOfWeek.Monday).ToString(),
                    ValueLabel = String.Format("{0:n}", cl.Sum(d => d.Amount)),

                });
                var tempList2 = rs.Where(e => e.Type == (int)Enums.FinanceEntry.EXPENSE
                && (CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(e.Date, CalendarWeekRule.FirstDay, DayOfWeek.Monday)
                == lastWeek || CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(e.Date, CalendarWeekRule.FirstDay, DayOfWeek.Monday) == CuurentWeek)).GroupBy(x => CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(x.Date, CalendarWeekRule.FirstDay, DayOfWeek.Monday)).OrderBy(r => r.First().Date).ThenBy(r2 => r2.First().Category).Select(cl => new Microcharts.Entry(float.Parse(cl.First().Amount.ToString()))
                {

                    Label = "Gastos Semana :" + CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(cl.First().Date, CalendarWeekRule.FirstDay, DayOfWeek.Monday).ToString(),
                    ValueLabel = String.Format("{0:n}", cl.Sum(d => d.Amount) * -1)
       ,
                });

                foreach (var item in tempList)
                {
                    var random = new Random();
                    var color = String.Format("#{0:X6}", random.Next(0x1000000));
                    while (colors.Contains(color))
                    {
                        color = String.Format("#{0:X6}", random.Next(0x1000000));
                    }
                    colors.Add(color);
                    Comparaties.Add(new Microcharts.Entry(float.Parse(item.ValueLabel))
                    {
                        Color = SkiaSharp.SKColor.Parse(color),
                        Label = item.Label,
                        ValueLabel = item.ValueLabel,
                    });
                }
                foreach (var item in tempList2)
                {
                    var random = new Random();
                    var color = String.Format("#{0:X6}", random.Next(0x1000000));
                    while (colors.Contains(color))
                    {
                        color = String.Format("#{0:X6}", random.Next(0x1000000));
                    }
                    colors.Add(color);
                    Comparaties.Add(new Microcharts.Entry(float.Parse(item.ValueLabel))
                    {
                        Color = SkiaSharp.SKColor.Parse(color),
                        Label = item.Label,
                        ValueLabel = item.ValueLabel,
                    });
                }
                foreach (var item in dates)
                {
                    financeEntries.AddRange(rs.Where(x => x.Date == item));
                }
                rs = financeEntries;
            }
            else if (TypeBankRange.SelectedIndex == 2)
            {

                string currentDate = Mes(DateTime.Now) + "-" + DateTime.Now.Year.ToString();
                string LastDate = Mes(DateTime.Now.AddMonths(-1)) + "-" + DateTime.Now.Year.ToString();
                var tempList = listFinance.Where(e => e.TypeFinanceEntry == (int)Enums.FinanceEntry.INCOME && (e.MonthYear == currentDate
                || e.MonthYear == LastDate)).GroupBy(x => x.MonthYear).OrderBy(r => r.First().Category).ThenBy(r2 => r2.First().MonthYear).Select(cl => new Microcharts.Entry(float.Parse(cl.First().AmountNumber.ToString()))
                {

                    Label = "Ingresos:" + cl.First().MonthYear,
                    ValueLabel = String.Format("{0:n}", cl.Sum(d => d.AmountNumber))
        ,
                });
                var tempList2 = listFinance.Where(e => e.TypeFinanceEntry == (int)Enums.FinanceEntry.EXPENSE && (e.MonthYear == currentDate
                || e.MonthYear == LastDate)).GroupBy(x => x.MonthYear).OrderBy(r => r.First().Category).ThenBy(r2 => r2.First().MonthYear).Select(cl => new Microcharts.Entry(float.Parse(cl.First().AmountNumber.ToString()))
                {

                    Label = "Gastos:" + cl.First().MonthYear,
                    ValueLabel = String.Format("{0:n}", cl.Sum(d => d.AmountNumber) * -1)
       ,
                });

                foreach (var item in tempList)
                {
                    var random = new Random();
                    var color = String.Format("#{0:X6}", random.Next(0x1000000));
                    while (colors.Contains(color))
                    {
                        color = String.Format("#{0:X6}", random.Next(0x1000000));
                    }
                    colors.Add(color);
                    Comparaties.Add(new Microcharts.Entry(float.Parse(item.ValueLabel))
                    {
                        Color = SkiaSharp.SKColor.Parse(color),
                        Label = item.Label,
                        ValueLabel = item.ValueLabel,
                    });
                }
                foreach (var item in tempList2)
                {
                    var random = new Random();
                    var color = String.Format("#{0:X6}", random.Next(0x1000000));
                    while (colors.Contains(color))
                    {
                        color = String.Format("#{0:X6}", random.Next(0x1000000));
                    }
                    colors.Add(color);
                    Comparaties.Add(new Microcharts.Entry(float.Parse(item.ValueLabel))
                    {
                        Color = SkiaSharp.SKColor.Parse(color),
                        Label = item.Label,
                        ValueLabel = item.ValueLabel,
                    });
                }
                rs = rs.Where(x => x.Date.Month == DateTime.Now.Month).ToList();
            }

            else if (TypeBankRange.SelectedIndex == 3)
            {
                var tempList = rs.Where(e => e.Type == (int)Enums.FinanceEntry.INCOME
           && (e.Date.Year == DateTime.Now.Year || e.Date.Year == DateTime.Now.Year - 1)).GroupBy(x => x.Date.Year).OrderBy(r => r.First().Date).ThenBy(r2 => r2.First().Category).Select(cl => new Microcharts.Entry(float.Parse(cl.First().Amount.ToString()))
           {

               Label = "Ingreso año  :" + cl.First().Date.Year,
               ValueLabel = String.Format("{0:n}", cl.Sum(d => d.Amount)),

           });
                var tempList2 = rs.Where(e => e.Type == (int)Enums.FinanceEntry.EXPENSE
             && (e.Date.Year == DateTime.Now.Year || e.Date.Year == DateTime.Now.Year - 1)).GroupBy(x => x.Date.Year).OrderBy(r => r.First().Date).ThenBy(r2 => r2.First().Category).Select(cl => new Microcharts.Entry(float.Parse(cl.First().Amount.ToString()))
             {

                 Label = "Gastos año  :" + cl.First().Date.Year,
                 ValueLabel = String.Format("{0:n}", cl.Sum(d => d.Amount) * -1)
       ,
             });

                foreach (var item in tempList)
                {
                    var random = new Random();
                    var color = String.Format("#{0:X6}", random.Next(0x1000000));
                    while (colors.Contains(color))
                    {
                        color = String.Format("#{0:X6}", random.Next(0x1000000));
                    }
                    colors.Add(color);
                    Comparaties.Add(new Microcharts.Entry(float.Parse(item.ValueLabel))
                    {
                        Color = SkiaSharp.SKColor.Parse(color),
                        Label = item.Label,
                        ValueLabel = item.ValueLabel,
                    });
                }
                foreach (var item in tempList2)
                {
                    var random = new Random();
                    var color = String.Format("#{0:X6}", random.Next(0x1000000));
                    while (colors.Contains(color))
                    {
                        color = String.Format("#{0:X6}", random.Next(0x1000000));
                    }
                    colors.Add(color);
                    Comparaties.Add(new Microcharts.Entry(float.Parse(item.ValueLabel))
                    {
                        Color = SkiaSharp.SKColor.Parse(color),
                        Label = item.Label,
                        ValueLabel = item.ValueLabel,
                    });
                }
                rs = rs.Where(x => x.Date.Year == DateTime.Now.Year).ToList();
            }
            else if (TypeBankRange.SelectedIndex == 4)
            {

                string currentDate = Mes(DateTime.Now) + "-" + DateTime.Now.Year.ToString();
                string LastDate = Mes(DateTime.Now.AddMonths(-1)) + "-" + DateTime.Now.Year.ToString();
                var tempList = listFinance.Where(e => e.TypeFinanceEntry == (int)Enums.FinanceEntry.INCOME && (e.MonthYear == currentDate
                || e.MonthYear == LastDate)).GroupBy(x => x.MonthYear).OrderBy(r => r.First().Category).ThenBy(r2 => r2.First().MonthYear).Select(cl => new Microcharts.Entry(float.Parse(cl.First().AmountNumber.ToString()))
                {

                    Label = "Ingresos:" + cl.First().MonthYear,
                    ValueLabel = String.Format("{0:n}", cl.Sum(d => d.AmountNumber))
        ,
                });
                var tempList2 = listFinance.Where(e => e.TypeFinanceEntry == (int)Enums.FinanceEntry.EXPENSE && (e.MonthYear == currentDate
                || e.MonthYear == LastDate)).GroupBy(x => x.MonthYear).OrderBy(r => r.First().Category).ThenBy(r2 => r2.First().MonthYear).Select(cl => new Microcharts.Entry(float.Parse(cl.First().AmountNumber.ToString()))
                {

                    Label = "Gastos:" + cl.First().MonthYear,
                    ValueLabel = String.Format("{0:n}", cl.Sum(d => d.AmountNumber) * -1)
       ,
                });

                foreach (var item in tempList)
                {
                    var random = new Random();
                    var color = String.Format("#{0:X6}", random.Next(0x1000000));
                    while (colors.Contains(color))
                    {
                        color = String.Format("#{0:X6}", random.Next(0x1000000));
                    }
                    colors.Add(color);
                    Comparaties.Add(new Microcharts.Entry(float.Parse(item.ValueLabel))
                    {
                        Color = SkiaSharp.SKColor.Parse(color),
                        Label = item.Label,
                        ValueLabel = item.ValueLabel,
                    });
                }
                foreach (var item in tempList2)
                {
                    var random = new Random();
                    var color = String.Format("#{0:X6}", random.Next(0x1000000));
                    while (colors.Contains(color))
                    {
                        color = String.Format("#{0:X6}", random.Next(0x1000000));
                    }
                    colors.Add(color);
                    Comparaties.Add(new Microcharts.Entry(float.Parse(item.ValueLabel))
                    {
                        Color = SkiaSharp.SKColor.Parse(color),
                        Label = item.Label,
                        ValueLabel = item.ValueLabel,
                    });
                }
                rs = rs.Where(x => x.Date >= DateStart.Date && x.Date <= DateEnd.Date).ToList();
            }
            foreach (var item in rs)
            {
                listFinance.Add(new FinanceEntryDto(item));
            }
            if (TypeCategoryFilter.SelectedIndex != -1)
            {
                listFinance = listFinance.Where(x => x.Category == TypeCategoryFilter.SelectedItem.ToString()).ToList();
            }
            if (TypeBankFilter.SelectedIndex != -1)
            {
                listFinance = listFinance.Where(x => x.Bank == TypeBankFilter.SelectedItem.ToString()).ToList();
            }

            DateTime dt = DateTime.Now;

            //list.ItemTemplate.SetValue(ImageCell.ImageSourceProperty, "Img");
            //list.ItemTemplate.SetBinding(ImageCell.ImageSourceProperty, new Binding("Img"));

            var entriesIncome = listFinance.Where(e => e.TypeFinanceEntry == (int)Enums.FinanceEntry.INCOME).GroupBy(x => x.Category).Select(cl => new Microcharts.Entry(float.Parse(cl.First().AmountNumber.ToString()))
            {


                Label = cl.First().Category,
                ValueLabel = String.Format("{0:n}", cl.Sum(d => d.AmountNumber))
          ,

            });
            var entriesExpenses = listFinance.Where(e => e.TypeFinanceEntry == (int)Enums.FinanceEntry.EXPENSE).GroupBy(x => x.Category).Select(cl => new Microcharts.Entry(float.Parse(cl.First().AmountNumber.ToString()))
            {


                Label = cl.First().Category,
                ValueLabel = String.Format("{0:n}", cl.Sum(d => d.AmountNumber))
         ,

            });

            if (TypeBankRange.SelectedIndex == -1)
            {
                string currentDate = Mes(DateTime.Now) + "-" + DateTime.Now.Year.ToString();
                string LastDate = Mes(DateTime.Now.AddMonths(-1)) + "-" + DateTime.Now.Year.ToString();
                var tempList = listFinance.Where(e => e.TypeFinanceEntry == (int)Enums.FinanceEntry.INCOME && (e.MonthYear == currentDate
                || e.MonthYear == LastDate)).GroupBy(x => x.MonthYear).OrderBy(r => r.First().Category).ThenBy(r2 => r2.First().MonthYear).Select(cl => new Microcharts.Entry(float.Parse(cl.First().AmountNumber.ToString()))
                {

                    Label = "Ingresos:" + cl.First().MonthYear,
                    ValueLabel = String.Format("{0:n}", cl.Sum(d => d.AmountNumber))
        ,
                });
                var tempList2 = listFinance.Where(e => e.TypeFinanceEntry == (int)Enums.FinanceEntry.EXPENSE && (e.MonthYear == currentDate
                || e.MonthYear == LastDate)).GroupBy(x => x.MonthYear).OrderBy(r => r.First().Category).ThenBy(r2 => r2.First().MonthYear).Select(cl => new Microcharts.Entry(float.Parse(cl.First().AmountNumber.ToString()))
                {

                    Label = "Gastos:" + cl.First().MonthYear,
                    ValueLabel = String.Format("{0:n}", cl.Sum(d => d.AmountNumber) * -1)
       ,
                });

                foreach (var item in tempList)
                {
                    var random = new Random();
                    var color = String.Format("#{0:X6}", random.Next(0x1000000));
                    while (colors.Contains(color))
                    {
                        color = String.Format("#{0:X6}", random.Next(0x1000000));
                    }
                    colors.Add(color);
                    Comparaties.Add(new Microcharts.Entry(float.Parse(item.ValueLabel))
                    {
                        Color = SkiaSharp.SKColor.Parse(color),
                        Label = item.Label,
                        ValueLabel = item.ValueLabel,
                    });
                }
                foreach (var item in tempList2)
                {
                    var random = new Random();
                    var color = String.Format("#{0:X6}", random.Next(0x1000000));
                    while (colors.Contains(color))
                    {
                        color = String.Format("#{0:X6}", random.Next(0x1000000));
                    }
                    colors.Add(color);
                    Comparaties.Add(new Microcharts.Entry(float.Parse(item.ValueLabel))
                    {
                        Color = SkiaSharp.SKColor.Parse(color),
                        Label = item.Label,
                        ValueLabel = item.ValueLabel,
                    });
                }
            }
            if (TypeBankRange.SelectedIndex == 1)
            {

            }

            foreach (var item in entriesIncome)
            {
                var random = new Random();
                var color = String.Format("#{0:X6}", random.Next(0x1000000));
                while (colors.Contains(color)) {
                    color = String.Format("#{0:X6}", random.Next(0x1000000));
                }
                colors.Add(color);
                Incomes.Add(new Microcharts.Entry(float.Parse(item.ValueLabel))
                {
                    Color = SkiaSharp.SKColor.Parse(color),
                    Label = item.Label,
                    ValueLabel = item.ValueLabel,
                });
            }
            foreach (var item in entriesExpenses)
            {
                var random = new Random();
                var color = String.Format("#{0:X6}", random.Next(0x1000000));
                while (colors.Contains(color))
                {
                    color = String.Format("#{0:X6}", random.Next(0x1000000));
                }
                colors.Add(color);
                Expenses.Add(new Microcharts.Entry(float.Parse(item.ValueLabel))
                {
                    Color = SkiaSharp.SKColor.Parse(color),
                    Label = item.Label,
                    ValueLabel = item.ValueLabel,
                });
            }
            chartIncome.Chart = new Microcharts.DonutChart { Entries = Incomes, LabelTextSize = 34 };

            chartExpenses.Chart = new Microcharts.DonutChart { Entries = Expenses, LabelTextSize = 34 };
            chartComparaties.Chart = new Microcharts.BarChart { Entries = Comparaties, LabelTextSize = 36 };
            DateTime firstDate = DateTime.Now;
            if (listFinance.Count > 0) { 
                 firstDate = listFinance.OrderBy(d => d.dateTime).First().dateTime;
            }
            int typeFinanceEntry = FinanceEntryType.SelectedIndex + 1;
            if (FinanceEntryType.SelectedIndex == -1)
            {
                typeFinanceEntry = 3;
            }
            decimal lastBalance = viewModel.GetLastBalance(firstDate, (Enums.FinanceEntry)typeFinanceEntry);
            foreach (var item in listFinance.OrderBy(d => d.dateTime))
            {
                lastBalance = Convert.ToDecimal(item.Amuount) + lastBalance;
                item.Balance = String.Format("{0:n}", lastBalance);
            }
            listViewm.ItemsSource = listFinance.OrderBy(d => d.dateTime);
        }
        private void btnCancelSearch_Clicked(object sender, EventArgs e)
        {
           bool visible = false;
            FinanceEntryType.SelectedIndex = -1;
            TypeBankFilter.SelectedIndex = -1;
            TypeBankRange.SelectedIndex = -1;
            TypeCategoryFilter.SelectedIndex = -1;
            FinanceEntryType.IsVisible = visible;
            //   TypeCategoryFilter.IsVisible = visible;
            TypeBankFilter.IsVisible = visible;
            TypeBankRange.IsVisible = visible;
            GridButton.IsVisible = visible;
            loadData();
        }
        private void btnCancel_Clicked(object sender, EventArgs e)
        {
            AmountE.Text = string.Empty;
            CommentarioE.Text = string.Empty;
            CatExpenses.Text = string.Empty;
            CategoryId = 0;
            FinanceEntryId = 0;
         
        }
        private void btnCancelarIncome_Clicked(object sender, EventArgs e)
        {
            Amount.Text = string.Empty;
            Commentario.Text = string.Empty;
            Cat.Text = string.Empty;
            // btnSelectCatIn.Text = "Categoria: ________________________";
            CategoryId = 0;
            FinanceEntryId = 0;
        }
        private string Mes(DateTime fecha)

        {



            string mes = "";



            switch (fecha.Month)

            {



                case 1:

                    mes =

                    "Enero";



                    break;



                case 2:

                    mes =

                    "Febrero";



                    break;



                case 3:

                    mes =

                    "Marzo";



                    break;



                case 4:

                    mes =

                    "Abril";



                    break;



                case 5:

                    mes =

                    "Mayo";



                    break;



                case 6:

                    mes =

                    "Junio";



                    break;



                case 7:

                    mes =

                    "Julio";



                    break;



                case 8:

                    mes =

                    "Agosto";



                    break;



                case 9:

                    mes =

                    "Septiembre";



                    break;



                case 10:

                    mes =

                    "Octubre";



                    break;



                case 11:

                    mes =

                    "Noviembre";



                    break;



                case 12:

                    mes =

                    "Diciembre";



                    break;

            };



            return mes;

        }

        private void listViewm_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var fincanceEntry = (FinanceEntryDto)e.SelectedItem;
            
            if (fincanceEntry.TypeFinanceEntry == (int)Enums.FinanceEntry.INCOME) {
                Amount.Text = fincanceEntry.AmountNumber.ToString();
                DateIncome.Date = Convert.ToDateTime( fincanceEntry.Date);
                FinanceEntryId = fincanceEntry.Id;
                bank1Income.SelectedItem = fincanceEntry.Bank;
                CategoryId = fincanceEntry.CategoryId;
                Cat.Text = "Categoria: " + fincanceEntry.Category;
                Commentario.Text = fincanceEntry.Comment;
                if (fincanceEntry.BankId2 > 0) {
                    bank2Income.IsVisible = true;
                    bank2Income.SelectedItem = fincanceEntry.Bank2;
                }
                this.CurrentPage = this.Children[0];
            }

            if (fincanceEntry.TypeFinanceEntry == (int)Enums.FinanceEntry.EXPENSE)
            {
                AmountE.Text = fincanceEntry.AmountNumber.ToString();
                DateExpense.Date = Convert.ToDateTime(fincanceEntry.Date);
                FinanceEntryId = fincanceEntry.Id;
                bank1Expenses.SelectedItem = fincanceEntry.Bank;
                CategoryId = fincanceEntry.CategoryId;
                CatExpenses.Text = "Categoria: " + fincanceEntry.Category;
                CommentarioE.Text = fincanceEntry.Comment;
                if (fincanceEntry.BankId2 > 0)
                {
                    bank2Expenses.IsVisible = true;
                    bank2Expenses.SelectedItem = fincanceEntry.Bank2;
                }
                this.CurrentPage = this.Children[1];
            }
        }
    }
}