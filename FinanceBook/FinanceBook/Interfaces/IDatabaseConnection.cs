﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinanceBook.Interfaces
{
   public interface IDatabaseConnection
    {
        SQLite.SQLiteConnection DbConnection();
    }
}
