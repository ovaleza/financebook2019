﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using System.ComponentModel;
using Xamarin.Forms;

namespace FinanceBook.Models
{
    [Table("Categories")]
    public class Categories : INotifyPropertyChanged
    {
        private int _id;
        [PrimaryKey, AutoIncrement]
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                this._id = value;
                OnPropertyChanged(nameof(Id));
            }
        }
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                this._name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        private int _type;
        public int Type
        {
            get
            {
                return _type;
            }
            set
            {
                this._type = value;
                OnPropertyChanged(nameof(Type));
            }
        }
        private string _details;
        public string Details
        {
            get
            {
                return _details;
            }
            set
            {
                this._details = value;
                OnPropertyChanged(nameof(Details));
            }
        }
        private bool _alowEdit;
        public bool AlowEdit {
            get
            {
                return _alowEdit;
            }
            set
            {
                this._alowEdit = value;
                OnPropertyChanged(nameof(AlowEdit));
            }
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this,
              new PropertyChangedEventArgs(propertyName));
        }
    }
}
