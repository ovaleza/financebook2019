﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using System.ComponentModel;
namespace FinanceBook.Models
{
    [Table("FinanceEntry")]
    public class FinanceEntry : INotifyPropertyChanged
    {
        private int _id;
        [PrimaryKey, AutoIncrement]
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                this._id = value;
                OnPropertyChanged(nameof(Id));
            }
        }
        private decimal _amount;
        [NotNull]
        public decimal Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                this._amount = value;
                OnPropertyChanged(nameof(Amount));
            }
        }
        private string _comment;
        public string Comment
        {
            get
            {
                return _comment;
            }
            set
            {
                this._comment = value;
                OnPropertyChanged(nameof(Comment));
            }
        }
        private int _type;
        public int Type
        {
            get
            {
                return _type;
            }
            set
            {
                this._type = value;
                OnPropertyChanged(nameof(Type));
            }
        }
        private int _category;
        public int Category
        {
            get
            {
                return _category;
            }
            set
            {
                this._category = value;
                OnPropertyChanged(nameof(Category));
            }
        }
        public int _bank1;
             public int Bank1
        {
            get
            {
                return _bank1;
            }
            set
            {
                this._bank1 = value;
                OnPropertyChanged(nameof(Bank1));
            }
        }
        public int _bank2;
        public int Bank2
        {
            get
            {
                return _bank2;
            }
            set
            {
                this._bank2 = value;
                OnPropertyChanged(nameof(Bank2));
            }
        }
        private DateTime _date;
        public DateTime Date
        {
            get
            {
                return _date;
            }
            set
            {
                this._date = value;
                OnPropertyChanged(nameof(Date));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this,
              new PropertyChangedEventArgs(propertyName));
        }
    }
}
