﻿
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace FinanceBook.Models
{
  public  class CategoriesCl
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public string Img { get; set; }
    }
}
