﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using System.ComponentModel;
using Xamarin.Forms;

namespace FinanceBook.Models
{
    [Table("Banks")]
    public class Bank
    {
        private int _id;
        [PrimaryKey, AutoIncrement]
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                this._id = value;
                OnPropertyChanged(nameof(Id));
            }
        }
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                this._name = value;
                OnPropertyChanged(nameof(Name));
            }
        }
        private string _reference;
        public string Reference
        {
            get
            {
                return _reference;
            }
            set
            {
                this._reference = value;
                OnPropertyChanged(nameof(Reference));
            }
        }
        private DateTime _date;
        public DateTime Date
        {
            get
            {
                return _date;
            }
            set
            {
                this._date = value;
                OnPropertyChanged(nameof(Date));
            }
        }

        private decimal _balance;
        public decimal Balance
        {
            get
            {
                return _balance;
            }
            set
            {
                this._balance = value;
                OnPropertyChanged(nameof(Balance));
            }
        }

        private string _comment;
        public string Comment
        {
            get
            {
                return _comment;
            }
            set
            {
                this._comment = value;
                OnPropertyChanged(nameof(Comment));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this,
              new PropertyChangedEventArgs(propertyName));
        }
    }
}
