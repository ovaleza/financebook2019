﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
namespace FinanceBook.Dtos
{
    public class FinanceEntryDto
    {
        public FinanceEntryDto(Models.FinanceEntry financeEntry )
        {
            ViewModels.CategoryViewModel categoryViewModel = new ViewModels.CategoryViewModel();
            ViewModels.BankViewModel bankViewModel = new ViewModels.BankViewModel();
            string simbol = string.Empty;
            if ((Enums.FinanceEntry)financeEntry.Type == Enums.FinanceEntry.EXPENSE)
            {
                this.Color = "Red";
                simbol = "-";
            }
            else
            {
                this.Color = "Green";
                simbol = "+";
            }
            this.Amuount = simbol + String.Format("{0:n}", financeEntry.Amount);
            this.Date = financeEntry.Date.ToShortDateString();
            if (financeEntry.Category == 0)
            {
                this.Category = categoryViewModel.GetCategories((Enums.Category)financeEntry.Type).First().Name;
            }
            else
            {
                this.Category = categoryViewModel.GetCategories((Enums.Category)financeEntry.Type).First(x => x.Id == financeEntry.Category).Name;
            }
            if (financeEntry.Bank1 == 0)
            {
                this.Bank = bankViewModel.GetBanks().First().Name;
            }
            else
            {
                try
                {
                    this.Bank = bankViewModel.GetBanks().First(x => x.Id == financeEntry.Bank1).Name;
                }
                catch (Exception)
                {
                    this.Bank = bankViewModel.GetBanks().First().Name;
                }
             
            }
            if (financeEntry.Bank2 == 0)
            {
                this.Bank2 = bankViewModel.GetBanks().First().Name;
            }
            else
            {
                try
                {
                    this.Bank2 = bankViewModel.GetBanks().First(x => x.Id == financeEntry.Bank2).Name;
                }
                catch (Exception)
                {
                    this.Bank2 = bankViewModel.GetBanks().First().Name;
                }

            }
            this.TypeFinanceEntry = financeEntry.Type;
            //this.Bank = bankViewModel.GetBanks().First().Name;
            this.AmountNumber = financeEntry.Amount;
            this.dateTime = financeEntry.Date;
            this.MonthYear = Mes(financeEntry.Date) +"-"+ financeEntry.Date.Year.ToString();
            this.Id = financeEntry.Id;
            this.CategoryId = financeEntry.Category;
            this.BankId1 = financeEntry.Bank1;
            this.BankId2 = financeEntry.Bank2;
            this.Comment = financeEntry.Comment;
           // this.Balance = "5,000.00";
        }
        private string Mes(DateTime fecha)

        {


            string mes = "";



            switch (fecha.Month)

            {



                case 1:

                    mes =

                    "Enero";



                    break;



                case 2:

                    mes =

                    "Febrero";



                    break;



                case 3:

                    mes =

                    "Marzo";



                    break;



                case 4:

                    mes =

                    "Abril";



                    break;



                case 5:

                    mes =

                    "Mayo";



                    break;



                case 6:

                    mes =

                    "Junio";



                    break;



                case 7:

                    mes =

                    "Julio";



                    break;



                case 8:

                    mes =

                    "Agosto";



                    break;



                case 9:

                    mes =

                    "Septiembre";



                    break;



                case 10:

                    mes =

                    "Octubre";



                    break;



                case 11:

                    mes =

                    "Noviembre";



                    break;



                case 12:

                    mes =

                    "Diciembre";



                    break;

            };



            return mes;

        }
        public string Amuount { get; set; }
        public string Date { get; set; }
        public string Category { get; set; }
        public string Bank { get; set; }
        public string Type { get; set; }
        public string Balance { get; set; }
        public string Color { get; set; }
        public decimal AmountNumber { get; set; }
        public DateTime dateTime { get; set; }
        public int TypeFinanceEntry { get; set; }
        public string MonthYear { get; set; }
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Comment { get; set; }
        public string Bank2 { get; set; }
        public int BankId1 { get; set; }
        public int BankId2 { get; set; }
    }
              
}
